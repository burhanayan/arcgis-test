import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { Pins } from './pins';
// const PINS_JSON = '../../assets/db/pins.json';
const PINS_JSON = 'http://localhost:3000/list';

@Injectable()
export class PinService {
  results: any;
  constructor(public http: HttpClient) {}

  // getPins(): Observable<Object> {
  //   return this.http.get(PINS_JSON);
  // }

  getPins() {
    return new Promise((resolve, reject) => {
      this.http
        .get(PINS_JSON)
        .toPromise()
        .then(
          (res: any[]) => {
            // Success
            // this.results = res;
            // this.results = res.json().results.map(item => {
            //   return new SearchItem(
            //       item.trackName,
            //       item.artistName,
            //       item.trackViewUrl,
            //       item.artworkUrl30,
            //       item.artistId
            //   );
            // });
            // this.results = res.json().results;
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  // addPin(pin: any): Observable<Object> {
  //   return this.http.post(PINS_JSON, pin);
  // }
  addPin(pin: any) {
    return new Promise((resolve, reject) => {
      this.http
        .post(PINS_JSON, pin)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  removeLast(): Observable<Object> {
    return this.http.delete(PINS_JSON + '/4');
  }
}
