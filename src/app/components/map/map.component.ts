import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { EsriLoaderService } from 'angular2-esri-loader';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  constructor(private esriLoader: EsriLoaderService) {}

  map: any;
  mapView: any;
  oid: string;

  @Output() onMapLoaded = new EventEmitter();
  @Output() onMarkClicked = new EventEmitter();

  ngOnInit() {
    this.esriLoader
      .load({
        url: '//js.arcgis.com/4.5'
      })
      .then(() => {
        this.esriLoader
          .loadModules(['esri/Map', 'esri/views/MapView'])
          .then(([Map, MapView, Graphic]) => {
            this.map = new Map({
              basemap: 'streets'
            });

            this.mapView = new MapView({
              container: 'esrimap',
              map: this.map
            });

            this.mapView.popup.on('trigger-action', (event) => {
              if (event.action.id === 'mark-donor') {
                this.oid = event.target.features[0].oid;
                this.onMarkClicked.emit();
              }
            });
            this.onMapLoaded.emit();
          });
      });
  }

  changeMapProperties(mapProperty) {
    for (const key in mapProperty) {
      if (mapProperty.hasOwnProperty(key)) {
        this.map[key] = mapProperty[key];
      }
    }
  }

  changeMapViewProperties(mapViewProperty) {
    for (const key in mapViewProperty) {
      if (mapViewProperty.hasOwnProperty(key)) {
        this.mapView[key] = mapViewProperty[key];
      }
    }
  }

  getOid() {
    return this.oid;
  }
}
