import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TopbarComponent } from './containers/topbar/topbar.component';
import { SidebarComponent } from './containers/sidebar/sidebar.component';
import { ContentComponent } from './containers/content/content.component';
import { MapComponent } from './components/map/map.component';

import { EsriLoaderService } from 'angular2-esri-loader';
import { PinService } from './services/pin.service';
import { ClickStopPropagationDirective } from './directives/click-stop-propagation.directive';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    SidebarComponent,
    ContentComponent,
    MapComponent,
    ClickStopPropagationDirective
  ],
  imports: [BrowserModule, FormsModule, HttpModule, HttpClientModule],
  providers: [EsriLoaderService, PinService],
  bootstrap: [AppComponent]
})
export class AppModule {}
