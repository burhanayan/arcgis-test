import { Component, ViewChild } from '@angular/core';
import { MapComponent } from '../../components/map/map.component';
import { PinService } from '../../services/pin.service';
import { ClickStopPropagationDirective } from '../../directives/click-stop-propagation.directive';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {
  pins: any[];
  pinData = {
    graphics: this.pins
    // graphics: [
    //   {
    //     id: 1,
    //     geometry: {
    //       type: 'point',
    //       longitude: 36,
    //       latitude: 40
    //     },
    //     symbol: {
    //       type: 'simple-marker',
    //       color: [226, 119, 40],
    //       outline: {
    //         color: [255, 255, 255],
    //         width: 2
    //       }
    //     },
    //     attributes: {
    //       Donor: 'Burhan Ayan',
    //       Organ: 'Limb',
    //       'Blood Type': 'B Rh(+)'
    //     },
    //     popupTemplate: {
    //       title: 'Details',
    //       content: [
    //         {
    //           type: 'fields',
    //           fieldInfos: [
    //             {
    //               fieldName: 'Donor'
    //             },
    //             {
    //               fieldName: 'Organ'
    //             },
    //             {
    //               fieldName: 'Blood Type'
    //             }
    //           ]
    //         }
    //       ]
    //     }
    //   },
    //   {
    //     id: 2,
    //     geometry: {
    //       type: 'point',
    //       longitude: 11,
    //       latitude: -44
    //     },
    //     symbol: {
    //       type: 'simple-marker',
    //       color: [226, 119, 40],
    //       outline: {
    //         color: [255, 255, 255],
    //         width: 2
    //       }
    //     }
    //   },
    //   {
    //     id: 3,
    //     geometry: {
    //       type: 'point',
    //       longitude: 21,
    //       latitude: -30
    //     },
    //     symbol: {
    //       type: 'simple-marker',
    //       color: [226, 119, 40],
    //       outline: {
    //         color: [255, 255, 255],
    //         width: 2
    //       }
    //     }
    //   }
    // ]
  };
  // {
  //   graphics: [
  //     {
  //       geometry: {
  //         type: 'point',
  //         longitude: 36,
  //         latitude: 40
  //       },
  //       symbol: {
  //         type: 'simple-marker',
  //         color: [226, 119, 40],
  //         outline: {
  //           color: [255, 255, 255],
  //           width: 2
  //         }
  //       },
  //       attributes: {
  //         Donor: 'Burhan Ayan',
  //         Organ: 'Limb',
  //         'Blood Type': 'B Rh(+)'
  //       },
  //       popupTemplate: {
  //         title: 'Details',
  //         content: [
  //           {
  //             type: 'fields',
  //             fieldInfos: [
  //               {
  //                 fieldName: 'Donor'
  //               },
  //               {
  //                 fieldName: 'Organ'
  //               },
  //               {
  //                 fieldName: 'Blood Type'
  //               }
  //             ]
  //           }
  //         ]
  //       }
  //     },
  //     {
  //       geometry: {
  //         type: 'point',
  //         longitude: 11,
  //         latitude: -44
  //       },
  //       symbol: {
  //         type: 'simple-marker',
  //         color: [226, 119, 40],
  //         outline: {
  //           color: [255, 255, 255],
  //           width: 2
  //         }
  //       }
  //     },
  //     {
  //       geometry: {
  //         type: 'point',
  //         longitude: 21,
  //         latitude: -30
  //       },
  //       symbol: {
  //         type: 'simple-marker',
  //         color: [226, 119, 40],
  //         outline: {
  //           color: [255, 255, 255],
  //           width: 2
  //         }
  //       }
  //     }
  //   ]
  // };

  additionalPin = {
    oid: 'XYZ',
    geometry: {
      type: 'point',
      longitude: 11,
      latitude: 75
    },
    symbol: {
      type: 'simple-marker',
      color: [120, 119, 40],
      outline: {
        color: [255, 255, 255],
        width: 4
      }
    },
    attributes: {
      Donor: 'Bahar Ayan',
      Organ: 'Liver',
      'Blood Type': 'AB Rh(+)'
    },
    popupTemplate: {
      title: 'Details',
      content: [
        {
          type: 'fields',
          fieldInfos: [
            {
              fieldName: 'Donor'
            },
            {
              fieldName: 'Organ'
            },
            {
              fieldName: 'Blood Type'
            }
          ]
        }
      ],
      actions: [
        {
          id: 'mark-donor',
          image: 'favicon.ico',
          title: 'Mark'
        }
      ]
    }
  };

  constructor(private pinservice: PinService) {}

  @ViewChild(MapComponent) map: MapComponent;

  loadMap() {
    // this.map.changeMapViewProperties(this.pinData);
    this.pinservice.getPins().then((val: any[]) => {
      this.pinData.graphics = val;
      this.map.changeMapViewProperties(this.pinData);
    });
    // this.pinservice.getPins().subscribe(data => {
    //   this.pinData.graphics = data;
    //   // this.map.changeMapViewProperties(this.pinList);
    //   this.map.changeMapViewProperties(this.pinData);
    // });
    // this.pinList = this.pinservice.results;
    // this.map.changeMapViewProperties(this.pinList);
  }

  // addPin() {
  //   console.log('Clicked to Add');
  //   this.pinservice.addPin(this.additionalPin).subscribe(() => {
  //   //   // this.pinList.graphics.push(this.additionalPin);
  //   //   // this.map.changeMapViewProperties(this.pinList);
  //   }, (err) => console.log(err));
  // }
  addPin() {
    // this.pinData.graphics.push(this.additionalPin);
    // this.map.changeMapViewProperties(this.pinData);
    // console.log('Clicked to Add');

    this.pinservice.addPin(this.additionalPin).then(val => {
      this.pinData.graphics.push(this.additionalPin);
      this.map.changeMapViewProperties(this.pinData);
    }).catch(err => {
      this.pinservice.addPin(err).then();
    });
  }

  addToMarkedList(event) {
    console.log(this.map.getOid());
  }

  removeLastPin() {
    this.pinservice.removeLast();
  }
}
